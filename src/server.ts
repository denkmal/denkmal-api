//require ('newrelic');
//require('@newrelic/koa');

import Koa from 'koa';
import cors from '@koa/cors';
import bodyParser from "koa-bodyparser";
import Router from '@koa/router';
import compress from 'koa-compress';
import {typeDefs} from 'api/typeDefs';
import {resolvers} from 'api/resolvers';
import {ApolloServer} from '@apollo/server';
import {ApolloServerPluginDrainHttpServer} from "@apollo/server/plugin/drainHttpServer";
import {koaMiddleware} from "@as-integrations/koa";
import {Level, WinstonLogger} from "./logger/logger";
import {GraphQLError} from 'graphql';
import {apolloAuthenticationContext} from "./api/authentication/apolloAuthenticationContext";
import http from "http";
import {getDataSource, initializeDataSource} from "./database/database";

const globalContext = global;

const server = async () => {
    let logLevel = process.env.LOG_LEVEL ? process.env.LOG_LEVEL : Level.info;
    let loggingOptions = {
        console: {level: logLevel},
    };
    if (process.env.ELASTICSEARCH_URL) {
        Object.assign(loggingOptions, {
            elasticsearch: {
                level: Level.info,
                host: process.env.ELASTICSEARCH_URL
            }
        });
    }
    const logger = new WinstonLogger(loggingOptions, {
        program: 'denkmal-api'
    });

    globalContext.logger = logger;


    logger.info("starting application");

    await initializeDataSource(false, logger);

    const app: Koa = new Koa();
    const httpServer = http.createServer(app.callback());

    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers,
        csrfPrevention: false,
        plugins: [ApolloServerPluginDrainHttpServer({httpServer})],
        formatError: (error : GraphQLError) => {
             logger.warn(`GraphQL error: ${error}`, {error});
             return error;
        }
    });
    await apolloServer.start();

    const router = new Router();
    router.get('/healthz', async (ctx, next) => {
        if (!getDataSource().isInitialized) {
            throw new Error("dataSource not initialized");
        }
        ctx.body = "";
    });


    app.use(async (ctx, next) => {
        logger.debug(`HTTP request: ${ctx.request.originalUrl}`);
        await next();
    });


    app.use(compress({
        threshold: 1024
    }));

    app.use(cors({origin: '*'}));
    app.use(bodyParser());

    router.all('/graphql', koaMiddleware(apolloServer, {
        context: apolloAuthenticationContext
    }));
    app.use(router.routes());

    const port = parseInt(process.env.PORT) || 5000;

    await httpServer.listen({port: port});

    logger.info("listening for requests");
};

server();
