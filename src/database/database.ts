import {Venue} from "../entities/venue";
import {Region} from "../entities/region";
import {Event} from "../entities/event";
import {URL} from 'url';

import {DataSource, DataSourceOptions} from 'typeorm';
import {EventVersion} from "../entities/eventVersion";
import {VenueAlias} from "../entities/venueAlias";
import {GenreCategory} from "../entities/genreCategory";
import {Genre} from "../entities/genre";
import {User} from "../entities/user";
import {Logger} from "../logger/logger";
import {TypeormLogger} from "../logger/typeormLogger";

let dataSource: DataSource = null;

async function createPostgresDataSource(connectionString?: string, ssl?: boolean, options?: Object): Promise<DataSource> {
    let host: string = 'localhost';
    let port: number = 5432;
    let username: string = 'postgres';
    let password: string = 'postgres';
    let database: string = 'postgres';

    if (connectionString) {
        const url = new URL(connectionString);
        host = url.hostname;
        port = parseInt(url.port);
        username = url.username;
        password = url.password;
        database = url.pathname && url.pathname.substring(1);
    }
    let databaseOptions: DataSourceOptions = {
        type: 'postgres',
        host: host,
        port: port,
        username: username,
        password: password,
        database: database,
        extra: {
            max: 50
        },
        entities: [
            Venue,
            Region,
            Event,
            EventVersion,
            VenueAlias,
            GenreCategory,
            Genre,
            User
        ],
        synchronize: false,
        migrationsRun: true,
        migrationsTableName: "migrations",
        migrations: ["src/migration/*.ts"]
    };

    if (ssl) {
        Object.assign(databaseOptions, {ssl: true});
        Object.assign(databaseOptions.extra, {
            ssl: {
                rejectUnauthorized: false
            }
        });
    }
    if (options) {
        Object.assign(databaseOptions, options);
    }

    return new DataSource(databaseOptions);
}

async function truncateAllTables() {

    const entities = [];
    (await (dataSource.entityMetadatas).forEach(
        x => entities.push({name: x.name, tableName: x.tableName})
    ));
    for (const entity of entities) {
        await dataSource.query(`TRUNCATE TABLE "${entity.tableName}" CASCADE`);
    }
}

async function initializeDataSource(test: boolean, logger: Logger = null): Promise<DataSource> {
    if (test) {
        let url = `postgres://postgres:postgres@localhost:5433/postgres`;
        if (process.env.DATABASE_HOST_TEST) {
            url = `postgres://postgres:postgres@${process.env.DATABASE_HOST_TEST}:5432/postgres`;
        }
        dataSource = await createPostgresDataSource(url, false, {
            dropSchema: true,
            logging: ['error']
        });
        await truncateAllTables();
    } else {
        let url = `postgres://postgres:postgres@localhost:5432/postgres`;
        if (process.env.RDS_POSTGRES_URL) {
            url = process.env.RDS_POSTGRES_URL;
        } else if (process.env.DATABASE_HOST) {
            url = `postgres://postgres:postgres@${process.env.DATABASE_HOST}:5432/postgres`;
        }
        dataSource = await createPostgresDataSource(url, !!process.env.RDS_POSTGRES_URL, {
            logger: new TypeormLogger(logger),
            logging: "all"
        });
    }

    await dataSource.initialize();

    return dataSource;
}

function getDataSource() {
    if (!dataSource) {
        throw new Error("dataSource used before initialization");
    }
    return dataSource;
}

export {getDataSource, initializeDataSource, truncateAllTables};
