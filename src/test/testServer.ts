import {AccessLevelEnum, User} from "../entities/user";
import {ApolloServer} from "@apollo/server";
import {typeDefs} from "../api/typeDefs";
import {resolvers} from "../api/resolvers";
import {MockLogger} from "../logger/logger";
import {getDataSource, initializeDataSource} from "../database/database";

const globalContext: any = global;

export const createTestServer = async function (accessLevel: AccessLevelEnum | undefined = undefined) {
    globalContext.logger = new MockLogger();

    await initializeDataSource(true);

    const entityManager = getDataSource().manager;
    let user = null;

    if (accessLevel) {
        user = new User();
        user.name = "test user";
        user.email = "test@test.com";
        user.password = "pw";
        user.accessLevel = accessLevel;
        await entityManager.save(user);
    }

    const apolloServer = new ApolloServer({
        typeDefs,
        resolvers
    });

    return {
        async executeOperation(operation, additionalContext = {}): Promise<any> {
            const response = await apolloServer.executeOperation({
                ...operation
            }, {
                contextValue: {...additionalContext, user: user}
            });

            // assume single response
            if (response.body.kind === 'single') {
                const data = response.body.singleResult.data;
                const errors = response.body.singleResult.errors;
                let obj = {
                    data: data
                };
                if (errors) {
                    obj["errors"] = errors;
                }
                return obj;
            } else {
                throw new Error('Unexpected response kind');
            }
        }
    }
};