import {MigrationInterface, QueryRunner} from "typeorm";

export class createIndexes1556285337644 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE INDEX "IDX_fdbb5d7c8101e2fc7cd74d7c8f" ON "eventVersions" ("isReviewPending") `);
        await queryRunner.query(`CREATE INDEX "IDX_59625af9d9d846ec1fe427148b" ON "genres" ("isReviewPending") `);
        await queryRunner.query(`CREATE INDEX "IDX_bfe0d74a2993d710726b341ea5" ON "venues" ("isReviewPending") `);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`DROP INDEX "IDX_bfe0d74a2993d710726b341ea5"`);
        await queryRunner.query(`DROP INDEX "IDX_59625af9d9d846ec1fe427148b"`);
        await queryRunner.query(`DROP INDEX "IDX_fdbb5d7c8101e2fc7cd74d7c8f"`);
    }

}
