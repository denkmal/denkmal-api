import {Region} from "../entities/region";
import {Venue} from "../entities/venue";
import {VenueAlias} from "../entities/venueAlias";
import {getDataSource, initializeDataSource} from "../database/database";
import {getVenueRepository} from "./venueRepository";

describe("venueRepository", () => {
    let region : Region;

    beforeAll(async () => {
        const dataSource = await initializeDataSource(true);

        region = new Region();
        region.slug = "venueRepository-test";
        region.name = "test region";
        region.latitude = 0;
        region.longitude = 0;
        region.email = "asd@adsf.com";
        await dataSource.manager.save(region);
    });

    afterAll(async () => {
        await getDataSource().destroy();
    });

    const addAliases = async function(manager, venue: Venue, aliases: string[]) {
        for(const aliasString of aliases) {
            const alias = new VenueAlias();
            alias.name = aliasString;
            alias.venue = venue;
            await manager.save(alias);
        }
    };

    test("findByNameOrAlias", async () => {
        const manager = getDataSource().manager;

        const venue1 = new Venue();
        venue1.name = "A test venue";
        venue1.region = Promise.resolve(region);
        await manager.save(venue1);

        const venue2 = new Venue();
        venue2.name = "The super-venue 2!";
        venue2.region = Promise.resolve(region);
        await manager.save(venue2);

        await addAliases(manager, venue1, ["another test", "blabla"]);
        await addAliases(manager, venue2, ["super VENUE", "blabläu"]);

        expect((await getDataSource().getRepository(VenueAlias).find()).length).toEqual(4);

        let venueRepository = getVenueRepository(getDataSource());
        expect(await venueRepository.findByNameOrAlias(region, "blablu")).toBeNull();
        expect((await venueRepository.findByNameOrAlias(region, "A test venue")).id).toEqual(venue1.id);
        expect((await venueRepository.findByNameOrAlias(region, "blabla")).id).toEqual(venue1.id);
        expect((await venueRepository.findByNameOrAlias(region, "blabläu")).id).toEqual(venue2.id);
        expect(await venueRepository.findByNameOrAlias(region, "")).toBeNull();
    });

    test("merge venue", async () => {
        const manager = getDataSource().manager;

        const venue1 = new Venue();
        venue1.name = "one";
        venue1.region = Promise.resolve(region);
        await manager.save(venue1);

        const venue2 = new Venue();
        venue2.name = "two";
        venue2.region = Promise.resolve(region);
        await manager.save(venue2);

        const venue1Id = venue1.id;
        const venue2Id = venue2.id;

        await addAliases(manager, venue1, ["eins", "uno"]);
        await addAliases(manager, venue2, ["zwei", "due"]);

        let venueRepository = getVenueRepository(getDataSource())
        await venueRepository.mergeVenue(venue1.id, venue2.id);

        const v1 = await venueRepository.findOne({where: { id: venue1Id}, loadEagerRelations: true});
        const v2 = await venueRepository.findOne({where: { id: venue2Id}, loadEagerRelations: true});

        expect(v1).toBeNull();
        expect(v2).toBeDefined();

        expect(v2.aliases).toHaveLength(5);
        expect(v2.aliases.find(a => a.name === "one")).toBeTruthy();
        expect(v2.aliases.find(a => a.name === "eins")).toBeTruthy();
        expect(v2.aliases.find(a => a.name === "uno")).toBeTruthy();
    });
});