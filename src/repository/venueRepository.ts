import {DataSource} from "typeorm";
import {Venue} from "../entities/venue";
import {Region} from "../entities/region";
import {VenueAlias} from "../entities/venueAlias";

export function getVenueRepository(dataSource: DataSource) {
    return dataSource.getRepository(Venue).extend({
        async findByNameOrAlias(region: Region, venueName: string): Promise<Venue | undefined> {
            return this.createQueryBuilder("venue")
                .leftJoinAndSelect("venue.aliases", "alias")
                .andWhere('"venue"."regionId" = :regionId', {regionId: region.id})
                .andWhere('("venue"."name" = :venueName OR "alias"."name" = :venueName)', {venueName: venueName})
                .getOne()
        },

        async mergeVenue(fromId: string, toId: string) {
            if (fromId === toId) {
                throw new Error("tried to merge venue into itself");
            }
            const fromVenue = await this.findOneOrFail({where: {id: fromId}, loadEagerRelations: true});
            const toVenue = await this.findOneOrFail({where: {id: toId}, loadEagerRelations: true});

            if (fromVenue.regionId !== toVenue.regionId) {
                throw new Error("tried to merge venues from different regions");
            }

            const venueAliasRepository = dataSource.getRepository(VenueAlias);

            // move events
            await dataSource.manager.query(`update events
                                      set "venueId" = $2
                                      where "venueId" = $1`, [fromVenue.id, toVenue.id]);

            // move aliases
            for (let alias of fromVenue.aliases) {
                alias.venue = toVenue;
                await venueAliasRepository.save(alias);
            }

            await venueAliasRepository.save(Object.assign(new VenueAlias(), {
                name: fromVenue.name,
                venue: toVenue
            }));

            await this.delete({id: fromVenue.id});
        }
    });
}