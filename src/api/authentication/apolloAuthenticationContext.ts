import {getUserWithToken} from "./token";

export const apolloAuthenticationContext = async (context) => {
    let currentUser = null;

    try {
        let authToken = context.ctx.req.headers.authentication;
        if (authToken.startsWith("Bearer ")) {
            currentUser = await getUserWithToken(authToken.substring(7));
        }
    } catch(error) {}

    return {
        user: currentUser
    };
}