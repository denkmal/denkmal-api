import {Brackets} from "typeorm";
import {Region} from "../../../entities/region";
import {queryBuilderWithListOptions} from "../../helpers/listOptionsToFindOptions";
import {getVenueRepository} from "../../../repository/venueRepository";
import {getDataSource} from "../../../database/database";

export const venuesListResolver = async function (obj, args, context, info) {
    let region = obj as Region;

    const repository = getVenueRepository(getDataSource());

    const alias = "venue";
    const lowercaseSort = ["name"];

    const listQuery = queryBuilderWithListOptions(repository, alias, args.listOptions, lowercaseSort).where(alias + ".region = :regionId", {regionId: region.id});
    listQuery.leftJoinAndSelect("venue.aliases", "aliases");

    if (args.search && args.search.length > 0) {
        listQuery.andWhere(new Brackets(query =>
            query.where("venue.name ILIKE :search", {search: args.search})
                .orWhere("aliases.name ILIKE :search", {search: args.search})
        ));
    }

    return {
        venues: listQuery.getMany(),
        count: listQuery.getCount()
    }
};
