import {queryBuilderWithListOptions} from "../../helpers/listOptionsToFindOptions";
import {AccessLevelEnum, User} from "../../../entities/user";
import {getDataSource} from "../../../database/database";

export const usersListResolver = async function (obj, args, context, info) {
    const repository = getDataSource().getRepository(User);

    const lowercaseSort = ["name", "email"];

    const alias = "user";

    let findQuery = queryBuilderWithListOptions(repository, alias, args.listOptions, lowercaseSort);
    if (context.user && context.user.accessLevel === AccessLevelEnum.Regional) {
        const region = await context.user.region;
        if (region) {
            findQuery.andWhere(alias + ".region = :regionId", {regionId: region.id});
        }
    }

    return {
        users: findQuery.getMany(),
        count: findQuery.getCount()
    }
};