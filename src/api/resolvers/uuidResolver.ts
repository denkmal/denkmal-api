import {GraphQLScalarType, Kind} from "graphql";
import {isUUID} from "validator";

export const uuidResolver = new GraphQLScalarType({
    name: 'ID',
    description: 'An RFC 4122 compatible UUID String',
    serialize(value) {
        return value;
    },
    parseValue(value) {
        return value;
    },
    parseLiteral(ast) {
        switch (ast.kind) {
            case Kind.STRING:
                if (isUUID(ast.value)) {
                    return ast.value
                }
        }
    }
});