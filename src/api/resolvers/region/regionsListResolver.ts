import {queryBuilderWithListOptions} from "../../helpers/listOptionsToFindOptions";
import {AccessLevelEnum} from "../../../entities/user";
import {getDataSource} from "../../../database/database";
import {Region} from "../../../entities/region";

export const regionsListResolver = async function (obj, args, context, info) {

    const repository = getDataSource().getRepository(Region);

    const alias = "region";

    const lowercaseSort = ["name"];

    let findQuery = queryBuilderWithListOptions(repository, alias, args.listOptions, lowercaseSort);

    if (context.user && context.user.accessLevel === AccessLevelEnum.Regional) {
        const region = await context.user.region;
        if (region) {
            findQuery.andWhere("id = :regionId", {regionId: region.id});
        }
    }

    return {
        regions: findQuery.getMany(),
        count: findQuery.getCount()
    }
};