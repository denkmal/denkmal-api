import {getDataSource} from "../../../database/database";
import {Region} from "../../../entities/region";

export const regionsResolver = async function (obj, args, context, info) {
    const repository = getDataSource().getRepository(Region);
    return await repository.find({order: {name: "ASC"}});
};