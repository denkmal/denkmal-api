import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";

export const genreCategoryResolver = async function (obj, {id}, context, info) {
    const repository = getDataSource().getRepository(GenreCategory);

    return repository.findOneOrFail({where: {id: id}});
};
