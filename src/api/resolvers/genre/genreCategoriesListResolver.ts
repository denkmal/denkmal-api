import {Region} from "../../../entities/region";
import {GenreCategory} from "../../../entities/genreCategory";
import {queryBuilderWithListOptions} from "../../helpers/listOptionsToFindOptions";
import {getDataSource} from "../../../database/database";

export const genreCategoriesListResolver = async function (region : Region, args, context, info) {
    const repository = getDataSource().getRepository(GenreCategory);

    const alias = "genreCategory";

    const listQuery = queryBuilderWithListOptions(repository, alias, args.listOptions).where(alias + ".region = :regionId", {regionId: region.id});

    return {
        genreCategories: listQuery.getMany(),
        count: listQuery.getCount()
    }
};
