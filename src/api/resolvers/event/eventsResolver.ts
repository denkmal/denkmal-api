import {ApiEvent} from "api/helpers/apiEvent";
import {Region} from "entities/region";
import {Venue} from "entities/venue";
import {getDataSource} from "../../../database/database";
import {getEventRepository} from "../../../repository/eventRepository";

export const eventsResolver = async function(obj, args, context, info) {
    let venue: Venue = null;
    let region: Region = null;

    // TODO: this is unsafe as the constructor.name might get minified
    if (obj && obj.constructor.name == "Venue") {
        region = await getDataSource().getRepository(Region).findOneOrFail({where: {id: obj.regionId}});
        venue = obj as Venue;
    }

    if (obj && obj.constructor.name == "Region") {
        region = obj as Region;
    }

    let eventDays = args.eventDays;

    if (!eventDays) {
        const [today] = region.getEventDayRangeNow();

        eventDays = [today];
        for(let i = 1; i <= 6; i++) {
            eventDays.push(today.clone().add(i, 'days'));
        }
    }

    return await Promise.all(
        eventDays.map(async eventDay => {
            const events = await getEventRepository(getDataSource()).findByEventDay(eventDay, region, venue, args.withHidden);
            return events.map(async e => await ApiEvent.apiEventForEvent(e));
        })
    );
};