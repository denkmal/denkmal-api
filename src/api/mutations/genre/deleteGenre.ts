import {Genre} from "../../../entities/genre";
import {getDataSource} from "../../../database/database";

export const deleteGenreMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const genreId = attrs.id;
    await entityManager.delete(Genre, {id: genreId});
    return true;
};