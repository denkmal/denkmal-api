import {Region} from 'entities/region';
import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";


export const addGenreCategoryMutation = async function (_, attrs) {
    const dataSource = getDataSource();
    const region = await dataSource.manager.findOneOrFail(Region, {where: {id: attrs.regionId}});

    const genreCategory = {
        ...attrs,
        region: Promise.resolve(region)
    };

    return await dataSource.manager.save(Object.assign(new GenreCategory(), genreCategory));
};
