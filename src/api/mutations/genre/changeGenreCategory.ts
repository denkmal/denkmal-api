import {GenreCategory} from "../../../entities/genreCategory";
import {getDataSource} from "../../../database/database";

export const changeGenreCategoryMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const genreCategory = await entityManager.findOneOrFail(GenreCategory, {where: { id: attrs.id}});

    return await entityManager.save(GenreCategory, {
        id: genreCategory.id,
        ...attrs
    });
};
