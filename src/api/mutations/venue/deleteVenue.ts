import {Venue} from "entities/venue";
import {getDataSource} from "../../../database/database";

export const deleteVenueMutation = async function (_, attrs) {
    const entityManager = getDataSource().manager;
    const venueId = attrs.id;
    await entityManager.delete(Venue, {id: venueId});
    return true;
};