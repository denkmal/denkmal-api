import {Venue} from 'entities/venue';
import {Region} from "entities/region";
import {getVenueRepository} from "../../../repository/venueRepository";
import {getDataSource} from "../../../database/database";


export const addVenueMutation = async function (_, attrs) {
    const venueRepository = getVenueRepository(getDataSource());
    const region = await getDataSource().manager.findOneOrFail(Region, {where: {id: attrs.regionId}});

    if (await venueRepository.findByNameOrAlias(region, attrs.name)) {
        throw new Error(`Duplicate venue name or alias ${attrs.name}`);
    }

    const venue = {
        ...attrs,
        region: Promise.resolve(region)
    }

    return await getDataSource().manager.save(Object.assign(new Venue(), venue));
};