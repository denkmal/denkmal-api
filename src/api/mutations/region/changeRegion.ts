import {Region} from 'entities/region';
import {getDataSource} from "../../../database/database";


export const changeRegionMutation = async function (_, attrs) {
        const entityManager = getDataSource().manager;
        const regionId = attrs.id;
        const region = await entityManager.findOneOrFail(Region, {where: {id: regionId}});

        return await entityManager.save(Region, {
            ...region,
            ...attrs
        });
    };
