import {ApiEvent} from "api/helpers/apiEvent";
import {getEventRepository} from "../../../repository/eventRepository";
import {getDataSource} from "../../../database/database";

export const hideEventMutation = async function (_, attrs) {
    const eventRepository = getEventRepository(getDataSource());
    const event = await eventRepository.findOneOrFail({where: {id: attrs.id}});

    event.isHidden = attrs.hide == true;

    await eventRepository.save(event);

    return ApiEvent.apiEventForEvent(event);
};