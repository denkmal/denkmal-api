import {createTestServer} from "../../../test/testServer";
import {gql} from 'graphql-tag'
import {AccessLevelEnum} from "../../../entities/user";
import moment = require("moment-timezone");
import {getDataSource} from "../../../database/database";
import {getEventRepository} from "../../../repository/eventRepository";

describe("suggestEvent", () => {
    let server;
    let regionSlug = "ser1";

    beforeEach(async () => {
        server = await createTestServer(AccessLevelEnum.Admin);

        await server.executeOperation({
            query: gql`
                mutation {addRegion(
                    name: "Suggest region"
                    slug: "${regionSlug}"
                    latitude: 40
                    longitude: 50
                    timeZone: "Europe/Zurich"
                    dayOffset: 5
                    email: "mail@asdf.ch"
                ){slug}}
            `
        });
    });

    afterEach(async () => {
        await getDataSource().destroy();
    });

    let getEvents = () => {
        const repository = getEventRepository(getDataSource());
        const query = repository.createQueryBuilder("event")
            .innerJoinAndSelect("event.activeVersion", "activeVersion")
            .leftJoinAndSelect("activeVersion.genres", "genres")
            .leftJoinAndSelect("genres.category", "category");
        return query.getMany();
    };

    test("suggest an event", async () => {
        let fromDate = moment.tz("Europe/Zurich").add(2, 'days');

        await server.executeOperation({
            query: gql`
                mutation {suggestEvent(
                    description: "Hey"
                    regionSlug: "${regionSlug}"
                    venueName: "the venue"
                    from: "${fromDate.toISOString()}"
                    genres: ["bla", "blu"]
                ){id}}
            `
        });

        {
            const events = await getEvents();
            expect(events).toHaveLength(1);
            const event = events[0];
            expect(event).toBeDefined();
            expect(event.activeVersion).toBeDefined();
            expect(event.activeVersion.genres).toHaveLength(2);
        }

        await server.executeOperation({
            query: gql`
                mutation {suggestEvent(
                    description: "Hey jo"
                    regionSlug: "${regionSlug}"
                    venueName: "another venue"
                    from: "${fromDate.toISOString()}"
                    genres: ["bla", "blu"]
                ){id}}
            `
        });

        {
            const events = await getEvents();
            expect(events).toHaveLength(2);
        }
    });
});