import {Event} from 'entities/event';
import {Venue} from 'entities/venue';
import {ApiEvent} from "api/helpers/apiEvent";
import {EventVersion, SourceTypeEnum} from "entities/eventVersion";
import {getDataSource} from "../../../database/database";
import {getEventRepository} from "../../../repository/eventRepository";


export const addEventMutation = async function(_, attrs) {
    const dataSource = getDataSource();
    const venue = await dataSource.manager.findOneOrFail(Venue, {where: {id: attrs.venueId}});

    const eventRepository = getEventRepository(dataSource);

    let event = await eventRepository.save(Object.assign(new Event(), {
        isHidden: attrs.isHidden,
        isPromoted: attrs.isPromoted,
        venue: Promise.resolve(venue)
    }));


    let genresObject = {};
    if (attrs.genres) {
        genresObject = {
            genres: attrs.genres.map(genreId => {return {id: genreId}})
        };
    }

    // create version
    const versionRepository = dataSource.getRepository(EventVersion);
    const version = await versionRepository.save(Object.assign(new EventVersion(), {
        ...attrs,
        isReviewPending: false,
        sourceType: SourceTypeEnum.Admin,
        event: Promise.resolve(event)
    }, genresObject
    ));

    event.activeVersion = version;
    const savedEvent = await eventRepository.save(event);

    // get event with all eager loaded data
    const query = eventRepository.createQueryBuilder("event")
        .where({id: savedEvent.id})
        .innerJoinAndSelect("event.activeVersion", "activeVersion")
        .leftJoinAndSelect("activeVersion.genres", "genres")
        .leftJoinAndSelect("genres.category", "category");

    // convert to api event
    return ApiEvent.apiEventForEvent(await query.getOne());
};
