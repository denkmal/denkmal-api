import {AccessLevelEnum, User} from "entities/user";
import {hashPassword} from "../../authentication/hashPassword";
import {Region} from "entities/region";
import {getDataSource} from "../../../database/database";

export const addUserMutation = async function (_, attrs, context) {
    const entityManager = getDataSource().manager;

    let region = null;
    if (attrs.regionId) {
        region = entityManager.findOneOrFail(Region, {where: {id: attrs.regionId}});
    }

    let values = {
        ...attrs
    };

    if (context.user && context.user.accessLevel === AccessLevelEnum.Regional) {
        if (!region || region.id !== context.user.regionId) {
            throw new Error("You can only create users in your region");
        }
        values.accessLevel = AccessLevelEnum.Regional;
    }

    const user = {
        ...values,
        password: hashPassword(values.password),
        region: region,
        regionId: region ? (await region).id : null
    }

    const savedUser = await entityManager.save(Object.assign(new User(), user));

    return entityManager.findOneOrFail(User, {where: {id: savedUser.id}});
};