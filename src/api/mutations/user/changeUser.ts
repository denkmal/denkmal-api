import {Region} from "entities/region";
import {hashPassword} from "../../authentication/hashPassword";
import {AccessLevelEnum, User} from "entities/user";
import {getDataSource} from "../../../database/database";

export const changeUserMutation = async function (_, attrs, context) {
    const userRepository = getDataSource().getRepository(User);

    const userId = attrs.id;
    const user = await userRepository.findOneOrFail({where: {id: userId}});

    let values = {
        ...attrs
    };

    if (context.user && context.user.accessLevel === AccessLevelEnum.Regional) {
        if (user.accessLevel !== AccessLevelEnum.Regional || !user.regionId || user.regionId !== context.user.regionId) {
            throw new Error("Not allowed");
        }

        values.accessLevel = AccessLevelEnum.Regional;
    }

    if (attrs.regionId) {
        const regionRepository = getDataSource().getRepository(Region);
        const region = await regionRepository.findOneOrFail({where: {id: attrs.regionId}});
        values.region = Promise.resolve(region);
        values.regionId = region.id;
    }
    if (attrs.password && attrs.password.length > 0) {
        values.password = hashPassword(attrs.password);
    } else {
        delete(values.password);
    }

    await userRepository.save({
        ...user,
        ...values,
        region: Promise.resolve(values.region)
    });

    // we need this because of a bug in typeorm where the lazy relation is not saved with .save()
    await userRepository.update({id: user.id}, {
        region: attrs.regionId
    });

    return userRepository.findOneOrFail({where: {id: user.id}});
}