import {AccessLevelEnum, User} from "entities/user";
import {getDataSource} from "../../../database/database";

export const deleteUserMutation = async function (_, attrs, context) {
    const entityManager = getDataSource().manager;
    const userId = attrs.id;
    const user = await entityManager.findOneOrFail(User, {where: { id: userId }});

    if (context.user && context.user.accessLevel === AccessLevelEnum.Regional) {
        if (user.accessLevel !== AccessLevelEnum.Regional || !user.regionId || user.regionId !== user.regionId) {
            throw new Error("Not allowed");
        }
    }

    await entityManager.delete(User, {id: userId});
    return true;
};