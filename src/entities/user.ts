import {
    BeforeInsert,
    Column,
    CreateDateColumn,
    Entity,
    ManyToOne,
    PrimaryGeneratedColumn,
    RelationId, Unique,
    UpdateDateColumn
} from "typeorm";
import {sharedDateTransformer} from "./transformers/dateTransformer";
import moment = require("moment-timezone");
import {IsEmail} from "class-validator";
import {Region} from "./region";
import {SourceTypeEnum} from "./eventVersion";

export enum AccessLevelEnum {
    Regional = "Regional",
    Admin = "Admin",
    Scraper = "Scraper"
}

@Entity('users')
@Unique(["email"])
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: moment.Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: moment.Moment;

    @Column('text')
    name: string;

    @Column('text')
    @IsEmail()
    email: string;

    @Column('text')
    password: string;

    @Column('text', {
        default: AccessLevelEnum.Regional
    })
    accessLevel: AccessLevelEnum;

    @Column('text')
    loginKey: string;

    @ManyToOne(type => Region, region => region.users, {
        onDelete: "CASCADE",
        nullable: true
    })
    region: Promise<Region>;

    @RelationId((user: User) => user.region)
    regionId: string;

    @BeforeInsert()
    async setLoginKey() {
        this.loginKey = "initial";
    }
}