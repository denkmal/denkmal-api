import {
    Entity,
    PrimaryGeneratedColumn,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne, Index, OneToMany, OneToOne, JoinColumn, Column, RelationId, BeforeInsert
} from 'typeorm';
import {Venue} from "./venue";
import {EventVersion} from "./eventVersion";
import {Moment} from "moment-timezone";
import {sharedDateTransformer} from "./transformers/dateTransformer";


@Entity('events')
export class Event {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @CreateDateColumn({transformer: sharedDateTransformer})
    createdAt: Moment;

    @UpdateDateColumn({transformer: sharedDateTransformer})
    updatedAt: Moment;

    @Column('boolean', {default: false, nullable: false})
    isHidden: boolean;

    @Column('boolean', {default: false, nullable: false})
    isPromoted: boolean;

    @OneToMany(type => EventVersion, eventVersion => eventVersion.event, {

    })
    versions: Promise<EventVersion[]>;

    @OneToOne(type => EventVersion, eventVersion => eventVersion.event, {
        eager: true,
        nullable: true
    })
    @JoinColumn()
    @Index()
    activeVersion: EventVersion;

    @RelationId((event: Event) => event.activeVersion)
    activeVersionId: string;

    @ManyToOne(type => Venue, venue => venue.events, {
        onDelete: "CASCADE",
        nullable: false
    })
    @Index()
    venue: Promise<Venue>;

    @RelationId((event: Event) => event.venue)
    venueId: string;

    // regionId as denormalized value
    @Column('uuid')
    regionId: string;

    @BeforeInsert()
    async setRegionId() {
        this.regionId = (await this.venue).regionId;
    }
}
